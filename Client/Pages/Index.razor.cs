﻿using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Authorization;
using MudBlazor;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using todoapp.Client.Services;
using todoapp.Shared.Model;

namespace todoapp.Client.Pages
{
    public partial class Index
    {
        public Category[] categorie { get; set; }
        public Todo[] todos { get; set; }
        [Inject]
        HttpClient _httpClient { get; set; }
        [Inject]
        NavigationManager navigationManager { get; set; }
        //[Inject]
        //CustomAuthStateProvider authStateProvider { get; set; }
        [Inject]
        ICategoryService categoryService { get; set; }
        [Inject]
        ITodoService todoService { get; set; }
        [Inject]
        ISnackbar Snackbar { get; set; }

        [CascadingParameter]
        Task<AuthenticationState> AuthenticationState { get; set; }

        /**protected async override Task OnInitializedAsync()
        {

            var name = (await AuthenticationState).User.Identity.Name;
            categorie = (await categoryService.GetCategoryAsync()).ToArray<Category>();
            todos = (await todoService.GetTodoAsync()).ToArray<Todo>();
            StateHasChanged();
        } **/

        async void CreateCategory(Category arg)
        {

            await categoryService.CreateCategoryAsync(arg);
            categorie = (await categoryService.GetCategoryAsync()).ToArray<Category>();
            StateHasChanged();
            Snackbar.Add("New Category Created!");

        }

        async void OnSelectedCategoryChanged(MudChip arg)
        {
            // update todo when selected category is changed
            var categoryId = arg.Tag.ToString();
            todos = (await _httpClient.GetFromJsonAsync<Todo[]>($"todo/gettodosbycategory/{categoryId}")).ToArray<Todo>();
            StateHasChanged();
        }
    }
}
