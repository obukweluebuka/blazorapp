using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using MudBlazor.Services;
using todoapp.Client.Services;
using Microsoft.AspNetCore.Components.Authorization;
using todoapp.Client.ViewModels;

namespace todoapp.Client
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var builder = WebAssemblyHostBuilder.CreateDefault(args);
            builder.RootComponents.Add<App>("#app");

            builder.Services.AddOptions();
            builder.Services.AddAuthorizationCore();
            builder.Services.AddScoped<CustomAuthStateProvider>();
            builder.Services.AddScoped<AuthenticationStateProvider>(s => s.GetRequiredService<CustomAuthStateProvider>());
            builder.Services.AddScoped<IAuthService, AuthenticationService>(); // inject services
            builder.Services.AddScoped<ICategoryService, CategoryService>(); // inject services
            builder.Services.AddScoped<ITodoService, TodoService>(); // inject services
            
            builder.Services.AddScoped(sp => new HttpClient { BaseAddress = new Uri(builder.HostEnvironment.BaseAddress) });
            builder.Services.AddScoped<IIndexViewModel, IndexViewModel>(); // inject services

            builder.Services.AddMudServices();

            await builder.Build().RunAsync();
        }
    }
}
