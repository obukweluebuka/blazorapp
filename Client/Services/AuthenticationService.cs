﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using todoapp.Shared.Model;

namespace todoapp.Client.Services
{
    public class AuthenticationService: IAuthService
    {
        private readonly HttpClient _httpClient;
        public AuthenticationService(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }
        public async Task<CurrentUser> CurrentUserInfo()
        {
            var result = await _httpClient.GetFromJsonAsync<CurrentUser>("api/authentication/currentuserinfo");
            return result;
        }
        public async Task Login(LoginModel loginRequest)
        {
            var result = await _httpClient.PostAsJsonAsync("api/authentication/login", loginRequest);
            if (result.StatusCode == System.Net.HttpStatusCode.BadRequest) throw new Exception(await result.Content.ReadAsStringAsync());
            result.EnsureSuccessStatusCode();
        }
        public async Task Logout()
        {
            var result = await _httpClient.PostAsync("api/authentication/logout", null);
            result.EnsureSuccessStatusCode();
        }
        public async Task Register(RegisterModel registerRequest)
        {
            var result = await _httpClient.PostAsJsonAsync("api/authentication/register", registerRequest);
            if (result.StatusCode == System.Net.HttpStatusCode.BadRequest) throw new Exception(await result.Content.ReadAsStringAsync());
            result.EnsureSuccessStatusCode();
        }
    }
}
