﻿
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using todoapp.Shared.Model;

namespace todoapp.Client.Services
{
    public class CategoryService : ICategoryService
    {

        private readonly HttpClient _httpClient;
        public CategoryService(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<Category[]> GetCategoryAsync()
        {
            var result = await _httpClient.GetFromJsonAsync<Category[]>("category");
            return result;
        }

        public async Task CreateCategoryAsync(Category category)
        {
            var result = await _httpClient.PostAsJsonAsync<Category>("api/category", category);
            result.EnsureSuccessStatusCode();
        }
    }
}
