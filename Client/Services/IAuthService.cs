﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using todoapp.Shared.Model;

namespace todoapp.Client.Services
{
    public interface IAuthService
    {
        Task Login(LoginModel loginRequest);
        Task Register(RegisterModel registerRequest);
        Task Logout();
        Task<CurrentUser> CurrentUserInfo();
    }
}
