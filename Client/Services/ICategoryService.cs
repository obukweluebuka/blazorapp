﻿using System.Threading.Tasks;
using todoapp.Shared.Model;

namespace todoapp.Client.Services
{
    public interface ICategoryService
    {
        Task CreateCategoryAsync(Category category);
        Task<Category[]> GetCategoryAsync();
    }
}