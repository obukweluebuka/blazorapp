﻿using System.Threading.Tasks;
using todoapp.Shared.Model;

namespace todoapp.Client.Services
{
    public interface ITodoService
    {
        Task CreateTodoAsync(Todo todo);
        Task<Todo[]> GetTodoAsync();
    }
}