﻿using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using todoapp.Shared.Model;

namespace todoapp.Client.Services
{
    public class TodoService : ITodoService
    {
        private readonly HttpClient _httpClient;
        public TodoService(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<Todo[]> GetTodoAsync()
        {
            var result = await _httpClient.GetFromJsonAsync<Todo[]>("todo/gettodos");
            return result;
        }

        public async Task CreateTodoAsync(Todo todo)
        {
            var result = await _httpClient.PostAsJsonAsync<Todo>("todo/createtodo", todo);
            result.EnsureSuccessStatusCode();
        }
    }
}
