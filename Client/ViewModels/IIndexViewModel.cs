﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MudBlazor;
using todoapp.Shared.Model;

namespace todoapp.Client.ViewModels
{
    interface IIndexViewModel
    {
        Category[] categorie { get; set; }
        Todo[] todos { get; set; }
        Task OnSelectedCategoryChanged(MudChip arg);
        Task CreateCategory(Category arg);

        Task OnInitializedViewModel();
    }
}
