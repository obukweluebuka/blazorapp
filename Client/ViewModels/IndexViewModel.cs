﻿using Microsoft.AspNetCore.Components;
using MudBlazor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using todoapp.Client.Services;
using todoapp.Shared.Model;

namespace todoapp.Client.ViewModels
{
    public class IndexViewModel : IIndexViewModel
    {
        private Category[] _categorie = new Category[] { };
        private Todo[] _todos = new Todo[] { };

        private readonly HttpClient _httpClient;
        private readonly NavigationManager _navigationManager;
        private readonly ICategoryService _categoryService;
        private readonly ITodoService _todoService;
        private readonly ISnackbar _snackbar;

     

        public IndexViewModel(HttpClient httpClient, NavigationManager navigationManager, ISnackbar snackbar, ICategoryService categoryService, ITodoService todoService)
        {
            _httpClient = httpClient;
            _navigationManager = navigationManager;
            _categoryService = categoryService;
            _todoService = todoService;
            _snackbar = snackbar;

            // OnInitializedViewModel().GetAwaiter().GetResult();
        }

        public Category[] categorie { get => _categorie; set => _categorie = value; }
        public Todo[] todos { get => _todos; set => _todos = value; }

        public async Task CreateCategory(Category arg)
        {
            await _categoryService.CreateCategoryAsync(arg);
            _categorie = (await _categoryService.GetCategoryAsync()).ToArray<Category>();
            _snackbar.Add("New Category Created!");
        }

        public async Task OnInitializedViewModel()
        {
            //await Task.Delay(0);
           _categorie = (await _categoryService.GetCategoryAsync()).ToArray<Category>();
           _todos = (await _todoService.GetTodoAsync()).ToArray<Todo>();
          // _categorie = new Category[] { new Category { CategoryName = "", UserId = new Guid("918343a5-f4ff-4c30-bcc5-f9b957349555"), Id = new Guid("918343a5-f4ff-4c30-bcc5-f9b957349556") } };
          //_todos = _todos = (await _httpClient.GetFromJsonAsync<Todo[]>($"todo/gettodos")).ToArray<Todo>();
            //_todos = new Todo[] { new Todo { CategoryId = new Guid("918343a5-f4ff-4c30-bcc5-f9b957349556"), Id = new Guid(), UserId = new Guid("918343a5-f4ff-4c30-bcc5-f9b957349555"), Completed = false, Description = "just", Title = "test", DueDate = DateTime.Now.ToLongDateString() } };
        }

        public async Task OnSelectedCategoryChanged(MudChip arg)
        {
            var categoryId = arg.Tag.ToString();
            _todos = (await _httpClient.GetFromJsonAsync<Todo[]>($"todo/gettodosbycategory/{categoryId}")).ToArray<Todo>();

        }
    }
}
