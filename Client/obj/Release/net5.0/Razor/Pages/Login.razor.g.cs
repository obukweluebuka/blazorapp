#pragma checksum "C:\Users\obukw\source\repos\todoapp\Client\Pages\Login.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "a1a823b3cb30a6cd3e6affef6e6e1ded11e2194f"
// <auto-generated/>
#pragma warning disable 1591
namespace todoapp.Client.Pages
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "C:\Users\obukw\source\repos\todoapp\Client\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\obukw\source\repos\todoapp\Client\_Imports.razor"
using System.Net.Http.Json;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Users\obukw\source\repos\todoapp\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\Users\obukw\source\repos\todoapp\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\Users\obukw\source\repos\todoapp\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\Users\obukw\source\repos\todoapp\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Web.Virtualization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\Users\obukw\source\repos\todoapp\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.WebAssembly.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\Users\obukw\source\repos\todoapp\Client\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\Users\obukw\source\repos\todoapp\Client\_Imports.razor"
using todoapp.Client;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "C:\Users\obukw\source\repos\todoapp\Client\_Imports.razor"
using todoapp.Client.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 11 "C:\Users\obukw\source\repos\todoapp\Client\_Imports.razor"
using todoapp.Client.Services;

#line default
#line hidden
#nullable disable
#nullable restore
#line 12 "C:\Users\obukw\source\repos\todoapp\Client\_Imports.razor"
using todoapp.Client.ViewModels;

#line default
#line hidden
#nullable disable
#nullable restore
#line 13 "C:\Users\obukw\source\repos\todoapp\Client\_Imports.razor"
using todoapp.Shared.Model;

#line default
#line hidden
#nullable disable
#nullable restore
#line 14 "C:\Users\obukw\source\repos\todoapp\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 15 "C:\Users\obukw\source\repos\todoapp\Client\_Imports.razor"
using MudBlazor;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\Users\obukw\source\repos\todoapp\Client\Pages\Login.razor"
using System.ComponentModel.DataAnnotations;

#line default
#line hidden
#nullable disable
    [Microsoft.AspNetCore.Components.LayoutAttribute(typeof(AuthLayout))]
    [Microsoft.AspNetCore.Components.RouteAttribute("/login")]
    public partial class Login : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __builder.OpenComponent<MudBlazor.MudPaper>(0);
            __builder.AddAttribute(1, "Elevation", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Int32>(
#nullable restore
#line 8 "C:\Users\obukw\source\repos\todoapp\Client\Pages\Login.razor"
                     0

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(2, "Class", "d-flex justify-center py-2 px-1 mt-6");
            __builder.AddAttribute(3, "ChildContent", (Microsoft.AspNetCore.Components.RenderFragment)((__builder2) => {
                __builder2.OpenComponent<MudBlazor.MudPaper>(4);
                __builder2.AddAttribute(5, "Elevation", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Int32>(
#nullable restore
#line 9 "C:\Users\obukw\source\repos\todoapp\Client\Pages\Login.razor"
                         0

#line default
#line hidden
#nullable disable
                ));
                __builder2.AddAttribute(6, "Class", "pa-2 mx-2");
                __builder2.AddAttribute(7, "ChildContent", (Microsoft.AspNetCore.Components.RenderFragment)((__builder3) => {
                    __builder3.OpenElement(8, "div");
                    __builder3.AddAttribute(9, "style", "max-width: 400px;");
                    __builder3.OpenComponent<Microsoft.AspNetCore.Components.Forms.EditForm>(10);
                    __builder3.AddAttribute(11, "Model", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Object>(
#nullable restore
#line 11 "C:\Users\obukw\source\repos\todoapp\Client\Pages\Login.razor"
                             model

#line default
#line hidden
#nullable disable
                    ));
                    __builder3.AddAttribute(12, "OnValidSubmit", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<Microsoft.AspNetCore.Components.EventCallback<Microsoft.AspNetCore.Components.Forms.EditContext>>(Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Forms.EditContext>(this, 
#nullable restore
#line 11 "C:\Users\obukw\source\repos\todoapp\Client\Pages\Login.razor"
                                                   OnValidSubmit

#line default
#line hidden
#nullable disable
                    )));
                    __builder3.AddAttribute(13, "ChildContent", (Microsoft.AspNetCore.Components.RenderFragment<Microsoft.AspNetCore.Components.Forms.EditContext>)((context) => (__builder4) => {
                        __builder4.OpenComponent<Microsoft.AspNetCore.Components.Forms.DataAnnotationsValidator>(14);
                        __builder4.CloseComponent();
                        __builder4.AddMarkupContent(15, "\r\n                ");
                        __builder4.OpenComponent<MudBlazor.MudCard>(16);
                        __builder4.AddAttribute(17, "ChildContent", (Microsoft.AspNetCore.Components.RenderFragment)((__builder5) => {
                            __builder5.OpenComponent<MudBlazor.MudCardContent>(18);
                            __builder5.AddAttribute(19, "ChildContent", (Microsoft.AspNetCore.Components.RenderFragment)((__builder6) => {
                                __Blazor.todoapp.Client.Pages.Login.TypeInference.CreateMudTextField_0(__builder6, 20, 21, "username", 22, "Username", 23, "Max. 8 characters", 24, 
#nullable restore
#line 16 "C:\Users\obukw\source\repos\todoapp\Client\Pages\Login.razor"
                                                                          () => model.UserName

#line default
#line hidden
#nullable disable
                                , 25, 
#nullable restore
#line 16 "C:\Users\obukw\source\repos\todoapp\Client\Pages\Login.razor"
                                                   model.UserName

#line default
#line hidden
#nullable disable
                                , 26, Microsoft.AspNetCore.Components.EventCallback.Factory.Create(this, Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.CreateInferredEventCallback(this, __value => model.UserName = __value, model.UserName)));
                                __builder6.AddMarkupContent(27, "\r\n                        ");
                                __Blazor.todoapp.Client.Pages.Login.TypeInference.CreateMudTextField_1(__builder6, 28, 29, "password", 30, "Password", 31, "Choose a strong password", 32, "mt-3", 33, 
#nullable restore
#line 18 "C:\Users\obukw\source\repos\todoapp\Client\Pages\Login.razor"
                                                                          () => model.Password

#line default
#line hidden
#nullable disable
                                , 34, 
#nullable restore
#line 18 "C:\Users\obukw\source\repos\todoapp\Client\Pages\Login.razor"
                                                                                                            InputType.Password

#line default
#line hidden
#nullable disable
                                , 35, 
#nullable restore
#line 18 "C:\Users\obukw\source\repos\todoapp\Client\Pages\Login.razor"
                                                   model.Password

#line default
#line hidden
#nullable disable
                                , 36, Microsoft.AspNetCore.Components.EventCallback.Factory.Create(this, Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.CreateInferredEventCallback(this, __value => model.Password = __value, model.Password)));
                                __builder6.AddMarkupContent(37, "\r\n                        ");
                                __Blazor.todoapp.Client.Pages.Login.TypeInference.CreateMudCheckBox_2(__builder6, 38, 39, "Remember Me", 40, 
#nullable restore
#line 19 "C:\Users\obukw\source\repos\todoapp\Client\Pages\Login.razor"
                                                    model.RememberMe

#line default
#line hidden
#nullable disable
                                , 41, Microsoft.AspNetCore.Components.EventCallback.Factory.Create(this, Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.CreateInferredEventCallback(this, __value => model.RememberMe = __value, model.RememberMe)));
                            }
                            ));
                            __builder5.CloseComponent();
                            __builder5.AddMarkupContent(42, "\r\n                    ");
                            __builder5.OpenComponent<MudBlazor.MudCardActions>(43);
                            __builder5.AddAttribute(44, "ChildContent", (Microsoft.AspNetCore.Components.RenderFragment)((__builder6) => {
                                __builder6.OpenComponent<MudBlazor.MudButton>(45);
                                __builder6.AddAttribute(46, "ButtonType", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<MudBlazor.ButtonType>(
#nullable restore
#line 22 "C:\Users\obukw\source\repos\todoapp\Client\Pages\Login.razor"
                                               ButtonType.Submit

#line default
#line hidden
#nullable disable
                                ));
                                __builder6.AddAttribute(47, "Variant", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<MudBlazor.Variant>(
#nullable restore
#line 22 "C:\Users\obukw\source\repos\todoapp\Client\Pages\Login.razor"
                                                                           Variant.Filled

#line default
#line hidden
#nullable disable
                                ));
                                __builder6.AddAttribute(48, "Color", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<MudBlazor.Color>(
#nullable restore
#line 22 "C:\Users\obukw\source\repos\todoapp\Client\Pages\Login.razor"
                                                                                                  Color.Primary

#line default
#line hidden
#nullable disable
                                ));
                                __builder6.AddAttribute(49, "Class", "ml-auto");
                                __builder6.AddAttribute(50, "ChildContent", (Microsoft.AspNetCore.Components.RenderFragment)((__builder7) => {
                                    __builder7.AddContent(51, "Login");
                                }
                                ));
                                __builder6.CloseComponent();
                            }
                            ));
                            __builder5.CloseComponent();
                        }
                        ));
                        __builder4.CloseComponent();
                        __builder4.AddMarkupContent(52, "\r\n                ");
                        __builder4.OpenComponent<MudBlazor.MudText>(53);
                        __builder4.AddAttribute(54, "Typo", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<MudBlazor.Typo>(
#nullable restore
#line 25 "C:\Users\obukw\source\repos\todoapp\Client\Pages\Login.razor"
                               Typo.body2

#line default
#line hidden
#nullable disable
                        ));
                        __builder4.AddAttribute(55, "Align", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<MudBlazor.Align>(
#nullable restore
#line 25 "C:\Users\obukw\source\repos\todoapp\Client\Pages\Login.razor"
                                                  Align.Center

#line default
#line hidden
#nullable disable
                        ));
                        __builder4.AddAttribute(56, "Class", "my-4");
                        __builder4.AddAttribute(57, "ChildContent", (Microsoft.AspNetCore.Components.RenderFragment)((__builder5) => {
                            __builder5.AddMarkupContent(58, "\r\n                    Fill out the form correctly to see the success message.\r\n                ");
                        }
                        ));
                        __builder4.CloseComponent();
                        __builder4.AddMarkupContent(59, "\r\n\r\n                ");
                        __builder4.OpenComponent<MudBlazor.MudExpansionPanels>(60);
                        __builder4.AddAttribute(61, "ChildContent", (Microsoft.AspNetCore.Components.RenderFragment)((__builder5) => {
                            __builder5.OpenComponent<MudBlazor.MudExpansionPanel>(62);
                            __builder5.AddAttribute(63, "Text", "Show Validation Summary");
                            __builder5.AddAttribute(64, "ChildContent", (Microsoft.AspNetCore.Components.RenderFragment)((__builder6) => {
#nullable restore
#line 31 "C:\Users\obukw\source\repos\todoapp\Client\Pages\Login.razor"
                         if (success)
                        {

#line default
#line hidden
#nullable disable
                                __builder6.OpenComponent<MudBlazor.MudText>(65);
                                __builder6.AddAttribute(66, "Color", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<MudBlazor.Color>(
#nullable restore
#line 33 "C:\Users\obukw\source\repos\todoapp\Client\Pages\Login.razor"
                                            Color.Success

#line default
#line hidden
#nullable disable
                                ));
                                __builder6.AddAttribute(67, "ChildContent", (Microsoft.AspNetCore.Components.RenderFragment)((__builder7) => {
                                    __builder7.AddContent(68, "Success");
                                }
                                ));
                                __builder6.CloseComponent();
#nullable restore
#line 34 "C:\Users\obukw\source\repos\todoapp\Client\Pages\Login.razor"
                        }
                        else
                        {

#line default
#line hidden
#nullable disable
                                __builder6.OpenComponent<MudBlazor.MudText>(69);
                                __builder6.AddAttribute(70, "Color", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<MudBlazor.Color>(
#nullable restore
#line 37 "C:\Users\obukw\source\repos\todoapp\Client\Pages\Login.razor"
                                             Color.Error

#line default
#line hidden
#nullable disable
                                ));
                                __builder6.AddAttribute(71, "ChildContent", (Microsoft.AspNetCore.Components.RenderFragment)((__builder7) => {
                                    __builder7.OpenComponent<Microsoft.AspNetCore.Components.Forms.ValidationSummary>(72);
                                    __builder7.CloseComponent();
                                }
                                ));
                                __builder6.CloseComponent();
#nullable restore
#line 40 "C:\Users\obukw\source\repos\todoapp\Client\Pages\Login.razor"
                        }

#line default
#line hidden
#nullable disable
                            }
                            ));
                            __builder5.CloseComponent();
                        }
                        ));
                        __builder4.CloseComponent();
                    }
                    ));
                    __builder3.CloseComponent();
                    __builder3.CloseElement();
                }
                ));
                __builder2.CloseComponent();
            }
            ));
            __builder.CloseComponent();
        }
        #pragma warning restore 1998
#nullable restore
#line 50 "C:\Users\obukw\source\repos\todoapp\Client\Pages\Login.razor"
       
    LoginModel model = new LoginModel();
    bool success;

    private async void OnValidSubmit(EditContext context)
    {
        try
        {
            await authStateProvider.Login(model);
            navigationManager.NavigateTo("");
           
        }
        catch (Exception)
        {

            success = false;
        }
    }


#line default
#line hidden
#nullable disable
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private CustomAuthStateProvider authStateProvider { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private NavigationManager navigationManager { get; set; }
    }
}
namespace __Blazor.todoapp.Client.Pages.Login
{
    #line hidden
    internal static class TypeInference
    {
        public static void CreateMudTextField_0<T>(global::Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder, int seq, int __seq0, System.Object __arg0, int __seq1, global::System.String __arg1, int __seq2, global::System.String __arg2, int __seq3, global::System.Linq.Expressions.Expression<global::System.Func<T>> __arg3, int __seq4, T __arg4, int __seq5, global::Microsoft.AspNetCore.Components.EventCallback<T> __arg5)
        {
        __builder.OpenComponent<global::MudBlazor.MudTextField<T>>(seq);
        __builder.AddAttribute(__seq0, "id", __arg0);
        __builder.AddAttribute(__seq1, "Label", __arg1);
        __builder.AddAttribute(__seq2, "HelperText", __arg2);
        __builder.AddAttribute(__seq3, "For", __arg3);
        __builder.AddAttribute(__seq4, "Value", __arg4);
        __builder.AddAttribute(__seq5, "ValueChanged", __arg5);
        __builder.CloseComponent();
        }
        public static void CreateMudTextField_1<T>(global::Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder, int seq, int __seq0, System.Object __arg0, int __seq1, global::System.String __arg1, int __seq2, global::System.String __arg2, int __seq3, global::System.String __arg3, int __seq4, global::System.Linq.Expressions.Expression<global::System.Func<T>> __arg4, int __seq5, global::MudBlazor.InputType __arg5, int __seq6, T __arg6, int __seq7, global::Microsoft.AspNetCore.Components.EventCallback<T> __arg7)
        {
        __builder.OpenComponent<global::MudBlazor.MudTextField<T>>(seq);
        __builder.AddAttribute(__seq0, "id", __arg0);
        __builder.AddAttribute(__seq1, "Label", __arg1);
        __builder.AddAttribute(__seq2, "HelperText", __arg2);
        __builder.AddAttribute(__seq3, "Class", __arg3);
        __builder.AddAttribute(__seq4, "For", __arg4);
        __builder.AddAttribute(__seq5, "InputType", __arg5);
        __builder.AddAttribute(__seq6, "Value", __arg6);
        __builder.AddAttribute(__seq7, "ValueChanged", __arg7);
        __builder.CloseComponent();
        }
        public static void CreateMudCheckBox_2<T>(global::Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder, int seq, int __seq0, global::System.String __arg0, int __seq1, T __arg1, int __seq2, global::Microsoft.AspNetCore.Components.EventCallback<T> __arg2)
        {
        __builder.OpenComponent<global::MudBlazor.MudCheckBox<T>>(seq);
        __builder.AddAttribute(__seq0, "Label", __arg0);
        __builder.AddAttribute(__seq1, "Checked", __arg1);
        __builder.AddAttribute(__seq2, "CheckedChanged", __arg2);
        __builder.CloseComponent();
        }
    }
}
#pragma warning restore 1591
