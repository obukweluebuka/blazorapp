module.exports = async (browser, context) => {
  // this is not a real login scenario. 
  const page = await browser.newPage();
  await page.goto('https://localhost:44345/login');
  await page.type('#username', 'vuvuzuela');
  await page.type('#password', 'Testpassword2021@');
  await page.screenshot({ path: 'example.png' });
  await Promise.all([
    page.click('[type="submit"]'), 
    page.waitForNavigation()]);
  await page.close();
};
