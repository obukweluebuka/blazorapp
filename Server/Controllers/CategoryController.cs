﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using todoapp.Server.Data;
using todoapp.Shared.Model;

namespace todoapp.Server.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        private readonly ApplicationDBContext _context;
        private readonly Guid _currentUserId;
        private readonly IHttpContextAccessor _httpContextAccessor;
        public CategoryController(ApplicationDBContext context, IHttpContextAccessor httpContextAccessor)
        {
            this._context = context;
            this._httpContextAccessor = httpContextAccessor;
            this._currentUserId = new Guid(_httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value);
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {

            var category = await _context.Category.Where(c => c.UserId == _currentUserId).ToListAsync();
            
           
            if (category == null)
            {
               await Post(new Category { CategoryName = "Default", UserId = _currentUserId });
               category = await _context.Category.Include(e => e.UserId.Equals(_currentUserId)).ToListAsync();
            }

            return Ok(category);
           
        }

        [HttpPost]
        public async Task<IActionResult> Post(Category category)
        {
            category.UserId = _currentUserId;
            _context.Add(category);
            await _context.SaveChangesAsync();
            return Ok(category);
        }


    }
}
