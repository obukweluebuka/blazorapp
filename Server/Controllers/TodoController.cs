﻿using Microsoft.AspNetCore.Mvc;
using System;
using todoapp.Shared.Model;
using todoapp.Server.Data;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;
using System.Linq;

namespace todoapp.Server.Controllers
{
    [Route("[controller]/[action]")]
    [ApiController]
    public class TodoController : ControllerBase
    {
        private readonly ApplicationDBContext _context;
        private readonly Guid _currentUserId;
        private readonly IHttpContextAccessor _httpContextAccessor;
        public TodoController(ApplicationDBContext context, IHttpContextAccessor httpContextAccessor)
        {
            this._context = context;
            this._httpContextAccessor = httpContextAccessor;
            this._currentUserId = new Guid(_httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value);
        }

        [HttpGet]
        public async Task<IActionResult> GetTodos()
        {
            var todo = await _context.Todo.Where(a => a.UserId == _currentUserId).ToListAsync();
            return Ok(todo);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetTodosByCategory(string id)
        {
            Console.WriteLine("Server response" + id);
            Console.WriteLine(".............................");
            var categoryId = new Guid(id);
            var todo = await _context.Todo.Where(a => a.UserId == _currentUserId && a.CategoryId == categoryId).ToListAsync();
            return Ok(todo);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetTodosById(string id)
        {
           
            var todoId = new Guid(id);
            var todo = await _context.Todo.Where(a => a.UserId == _currentUserId && a.Id == todoId).ToListAsync();
            return Ok(todo);
        }


        [HttpPost]
        public async Task<IActionResult> CreateTodo(Todo todo)
        {
            todo.UserId = _currentUserId;
            _context.Add(todo);
            await _context.SaveChangesAsync();
            return Ok(todo);
        }

        [HttpPut]
        public async Task<IActionResult> UpdateTodo(Todo todo)
        {
            _context.Entry(todo).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            return NoContent();
        }

    }
}
