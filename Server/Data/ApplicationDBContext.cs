﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using todoapp.Server.Models;
using todoapp.Shared.Model;

namespace todoapp.Server.Data
{
    public class ApplicationDBContext: IdentityDbContext<ApplicationUser>
    {
        public ApplicationDBContext(DbContextOptions<ApplicationDBContext> options) : base(options)
        {
        }
        public DbSet<Category> Category { get; set; }
        public DbSet<Todo> Todo { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Category>().HasData(
                new Category
                {
                    Id = Guid.NewGuid(),
                    CategoryName= "undefined"

                }) ;

            modelBuilder.Entity<Todo>().HasData(
               new Todo
               {
                   Id = Guid.NewGuid(),
                   Title = "Visit the doctor tomorrow",
                   Completed = false,
                   DueDate = DateTime.Today.ToLongDateString(),

               });
        }
    }
}
