﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace todoapp.Shared.Model
{
    public class Category
    {
        public Guid Id { set; get; }
        public string CategoryName { set; get; }
        [Required]
        public Guid UserId { set; get; }
    }
}