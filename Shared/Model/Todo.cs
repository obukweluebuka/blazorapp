﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace todoapp.Shared.Model
{
    public class Todo
    {
        public Guid Id { get; set; }

        [Required]
        [StringLength(200, ErrorMessage = "Title length can't be more than 200.")]
        public string Title { get; set; }

        [Required]
        public Guid CategoryId { get; set; }

        [Required]
        public Guid UserId { get; set; }

        [Required]
        public string DueDate { get; set; }

        public bool Completed { get; set; }

        public string Description { get; set; }
    }
}
