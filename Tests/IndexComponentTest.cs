using Bunit;
using Bunit.TestDoubles;
using Moq;
using System;
using System.Threading.Tasks;
using todoapp.Client.Services;
using Xunit;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.Extensions.DependencyInjection;
using MudBlazor.Services;
using Microsoft.AspNetCore.Components;
using todoapp.Shared.Model;
using Bunit.Rendering;
using System.Security.Claims;
using RichardSzalay.MockHttp;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.Json;

namespace Tests
{
    public class TestFixture : IDisposable
    {
        public void SetUp(TestContextBase context)
        {

            // Mock Services
            var TodoServiceMoq = new Mock<ITodoService>();
            var CategpryServiceMoq = new Mock<ICategoryService>();


            // Fake Authorization
            var userId = new Guid("{5d5fa9c1-abf9-4ed6-8fb0-3365382b629c}");
            var authContext = context.AddTestAuthorization();
            var emailClaim = new Claim(ClaimTypes.Email, "user@test.com");
            var uuidClaim = new Claim(ClaimTypes.Sid, userId.ToString());
            authContext.SetAuthorized("TestUser").SetClaims(uuidClaim, emailClaim);

            // Register Services
            context.Services.AddScoped<CustomAuthStateProvider>();
            context.Services.AddSingleton<IAuthService, AuthenticationService>();
            context.Services.AddSingleton<ITodoService>(TodoServiceMoq.Object);
            context.Services.AddSingleton<ICategoryService>(CategpryServiceMoq.Object);
            context.Services.AddSingleton<NavigationManager, FakeNavigationManager>();
            context.Services.AddMudServices();

            TodoServiceMoq.Setup(m => m.GetTodoAsync()).Returns(
               Task.FromResult(
                   new Todo[]
                   {
                        new Todo()
                        {
                        Title = "Test title",
                        Completed = false,
                        Description = "",
                        DueDate = DateTime.Today.ToString("d-MM-yyyy"),
                        UserId = userId,
                        CategoryId = new Guid("{5d5fa9c1-abf9-4ed6-8fb0-3365382b629a}"),        }
                   }));

            CategpryServiceMoq.Setup(m => m.GetCategoryAsync()).Returns(
                Task.FromResult(
                    new Category[]
                    {
                        new Category()
                        {
                        CategoryName = "Default",
                        Id = new Guid("{5d5fa9c1-abf9-4ed6-8fb0-3365382b629a}"),
                        }
                    }));
        }
        public void Dispose()
        {
           
        }
    }
    public class IndexComponentTest : TestContext, IClassFixture<TestFixture>
    {
        public IndexComponentTest(TestFixture fixture)
        {
            fixture.SetUp(this);
        }
        [Fact]
        public void UserShouldBeAuthenticated()
        {
            var cut = RenderComponent<todoapp.Client.Pages.Index>();
            var user = cut.Find("#userWelcome");
            Assert.Matches(user.TextContent, "Hello TestUser!");
        
        }

        [Fact]
        public void CategoryShouldIncludeDefault()
        {
            var cut = RenderComponent<todoapp.Client.Pages.Index>();
            Assert.Equal("Default", cut.Instance.categorie[0].CategoryName);
        }

        [Fact]
        public void CategoryCountShouldBeOne()
        {
            var cut = RenderComponent<todoapp.Client.Pages.Index>();
            var categories = cut.FindAll(".mud-chipset .mud-chip");
            Assert.Equal(2, categories.Count);
        }

        [Fact]
        public void TodoListShouldNotBeEmpty()
        {
            var cut = RenderComponent<todoapp.Client.Pages.Index>();
            var categories = cut.FindAll(".mud-list .mud-list-item");
            Assert.Equal(1, categories.Count);
        }

        [Fact (Skip = "I am too lazy to run now, please skip")]
        public void TodoListMarkupShouldNotChange()
        {
            var cut = RenderComponent<todoapp.Client.Pages.Index>();
            var expectedHtml = @"<div></div>";
            cut.MarkupMatches(expectedHtml);
        }
    }
}
